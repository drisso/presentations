---
title: "Normalization, clustering, and differential expression of single-cell RNA-seq data"
author: "davide.risso@berkeley.edu"
date: "August 19, 2016"
output: 
  ioslides_presentation:
    widescreen: true
    smaller: false
    incremental: false
    fig_width: 8
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, cache=TRUE, results="hide", error=FALSE, message=FALSE, warning=FALSE, fig.align="center")
options(getClass.msg=FALSE)
```

## Outline {.emphasized}

* Motivation
* Experimental design: batch effects and confounding
* Normalization: accounting for sample quality
* Clustering: resampling and sequential strategies

\

\

<center> slides available at: http://rpubs.com/daviderisso/iisa2016 </center>

## Illuminating cellular diversity in the brain

<div class="centered">
<img src="../common_figures/ramon_cajal.png" width="80%"/>
</div>

The brain is made up of 100’s if not 1000’s different cell types. \
We need a rational way to identify and classify them.


## Single-cell sequencing of S1 cortex


<div class="centered">
<img src="../common_figures/layers.png" width="80%"/>
</div>

* 285 Layer 4 cells 
* 657 Layer 5 cells
* 307 Layer 6 cells

# Experimental design: batch effects and confounding

## Experimental process

<div class="centered">
<img src="../common_figures/experimental_process.png" width="80%"/>
</div>

Low sequencing depth: 192 cells per Illumina lane (average 1.2M reads per cell)


## Snapshot of the data

 _ | Olfactory | Cortical
:----: | :----: | :----:
Mice | 51 | 41
C1 runs | 61 | 40
Illumina Lanes | 19 | 7
Cells | 2,627 | 1,249
Cells pass QC | 2,190 | 1,042
Sequenced reads | 4,000 M | 1,500 M

## Ideal design: Factorial experiment

Each level of the factor of interest, say layer of origin, is observed in each batch.

<div class="centered">
<img src="../common_figures/design1.png" width="80%"/>
</div>

## Technical limitation: Complete confounding

We can only isolate one cell type per animal / batch.

<div class="centered">
<img src="../common_figures/design2.png" width="80%"/>
</div>

<sub><sup>
See also in bioRxiv: \
Hicks et al. (2015) http://dx.doi.org/10.1101/025528
</sub></sup>

## Partial solution: Replication

Need to have multiple batches per condition and account for batch effects in the design matrix (nested design).

<div class="centered">
<img src="../common_figures/design3.png" width="80%"/>
</div>

Note that mouse and C1 run effects are still confounded!

\

<sub><sup>
See also in bioRxiv: \
Tung et al. (2016) http://dx.doi.org/10.1101/062919
</sub></sup>

## Partial solution: Nested design

To account for batch $j(i)$ in condition $i$, we can model the log-expression of each sample $k$ as

$$
y_{ijk} = \mu + \alpha_i + \beta_{j(i)} + \varepsilon_{ijk},
$$

subject to the $n+1$ constraints

$$
\sum_{i=1}^n \alpha_i = 0; \\
\sum_{j=1}^{n_i} \beta_{j(i)} = 0.
$$

## Sample quality influences gene expression

```{r qc_cor}
library(brainData)
library(scone)
library(clusterExperiment)
data(cortical)

filterCount <- function(counts, nRead=5, nCell=5){
  filter <- apply(counts, 1, function(x) length(x[x>=nRead])>=nCell)
  return(filter)
}

l456 <- cortical[, colData(cortical)$MD_single_bulk_pool=="S"]
tpms <- assay(l456, 3)

filter <- filterCount(tpms)
tpms <- tpms[filter,]

filter <- metric_sample_filter(tpms, nreads=colData(l456)$NREADS, ralign=colData(l456)$RALIGN)
idx <- which(!filter$filtered_nreads | filter$filtered_ralign)

mat <- assay(l456[rownames(tpms),idx], 3)
batch <- droplevels(colData(l456[,idx])$MD_c1_run_id)
pca <- prcomp(t(log1p(mat)), center=TRUE, scale=TRUE)

qc <- as.matrix(colData(l456[,idx])[,metadata(l456[,idx])$which_qc])
colnames(qc) <- metadata(l456)$which_qc
qcpca <- prcomp(qc, center=TRUE, scale=TRUE)

cors <- sapply(1:10, function(i) cor(pca$x[,i], qc))
colnames(cors) <- paste("PC", 1:NCOL(cors), sep="")
rownames(cors) <- paste("QC", 1:NROW(cors), sep="")
barplot(abs(cors), beside = TRUE, col=bigPalette[1:16], border=bigPalette[1:16], ylim=c(0, 1), space=c(0, 4), ylab="Absolute correlation")
legend("topright", colnames(qc), fill=bigPalette, border=bigPalette, cex=.7)
```
Absolute correlation between PCA of log(TPM+1) and QC scores.

## Sample quality differs among batches

```{r qc_batch}
plot(qcpca$x[,1]~batch, las=2, xlab="", ylab="QC PC1", col=bigPalette, varwidth = FALSE)
```
PC1 of the QC score matrix stratified by batch.

# Normalization: accounting for sample quality

## A general normalization framework

\

<div class="centered">
<img src="../common_figures/ruv_model.png" width="100%"/>
</div>

RUV can be used to estimate $W \alpha$ using negative control genes.

<sub><sup>
Gagnon-Bartsch & Speed (2012) [http://dx.doi.org/10.1093/biostatistics/kxr034] \
Risso et al. (2014) [http://dx.doi.org/10.1038/nbt.2931] \
RUVSeq package [http://bioconductor.org/packages/RUVSeq/]
</sub></sup>

## RUV -- negative control genes (RUVg) {.smaller}

1.  **Identify a subset of negative control genes**, i.e., non-DE genes, for which $\beta_c = 0$.  Then,
		$$
		\log E[Y_c | W, X] = W \, \alpha_c.
		$$
2. **Perform the singular value decomposition (SVD)** of $\log Y_c$,
		$$
		\log Y_c = U \Lambda V^T.
		$$
		For a given $k$, estimate $W$ by 
		$$
		\widehat{W} = U \Lambda_k.
		$$
3. **Substitute $\widehat{W}$** into the model for the full set of $J$ genes and  **estimate both $\alpha$ and $\beta$ by GLM regression**.
4. (Optionally) Define normalized read counts as the residuals from ordinary least squares (OLS) regression of $\log Y$ on $\widehat{W}$.

<sub><sup>
Risso et al. (2014) [http://dx.doi.org/10.1038/nbt.2931] \
RUVSeq package [http://bioconductor.org/packages/RUVSeq/]
</sub></sup>

## RUV captures sample quality {.smaller}

```{r ruv, fig.width=10}
library(RUVSeq)
data("housekeeping")
mat <- assay(l456[rownames(tpms),idx])
mat <- mat[rowMeans(mat)>0,]
fq <- betweenLaneNormalization(mat, which="full")
fq <- fq[rowMeans(fq)>0,]
hk <- intersect(rownames(fq), tools::toTitleCase(tolower(housekeeping[,1])))
r <- RUVg(fq, hk, k=2)

layer <- droplevels(colData(l456[,idx])$MD_cell_type)
collayer <- c(RColorBrewer::brewer.pal(3, "Set1")[1:2], RColorBrewer::brewer.pal(6, "Set2")[6])

layout(matrix(c(1,1,2,3), 2, 2, byrow = FALSE))
plot(r$W[layer!="NTSR1",1], qcpca$x[layer!="NTSR1",1], pch=19, col=collayer[layer[layer!="NTSR1"]], xlab="RUV factor", ylab="QC factor")
legend("topleft", paste0("Pearson correlation: ", round(cor(r$W[layer!="NTSR1",1], qcpca$x[layer!="NTSR1",1]), 2)))
legend("bottomright", levels(layer)[1:2], fill=collayer)

plot(density(r$W[layer=="SCNN1A", 1]), col=collayer[1], lwd=2, xlim=c(-.07, .12), main="RUV factor")
lines(density(r$W[layer=="RBP4", 1]), col=collayer[2], lwd=2)
plot(density(qcpca$x[layer=="SCNN1A",1]), col=collayer[1], lwd=2, xlim=c(-7, 10), main="QC factor")
lines(density(qcpca$x[layer=="RBP4", 1]), col=collayer[2], lwd=2)
```

<center> 
Should we scale the data before RUV? \
QC or RUV? How many factors? Batch?
</center>

## SCONE

1. Apply a (combination of) normalization method(s).
  
    * Global scaling, e.g., DESeq, TMM.
    * Full-quantile (FQ)
    * Unknown factors of unwanted variation, e.g., RUV.
    * Known factors of unwanted variation, e.g., regression on QC measures, C1 batch.
  
2. Rank the normalizations using a set of **performance scores**.

<center> 
R package available at https://github.com/YosefLab/scone
</center>

\
\

<sub><sup>
Michael Cole, Nir Yosef, Sandrine Dudoit
</sub></sup>

## SCONE performance metrics

* **BIO_SIL**: Average silhouette width by biological condition.
* **BATCH_SIL**: Average silhouette width by batch.
* **PAM_SIL**: Maximum average silhouette width for PAM clusterings, for a range of user-supplied numbers of clusters.
* **EXP_QC_COR**: Maximum squared Spearman correlation between count PCs and QC measures.
* **EXP_UV_COR**: Maximum squared Spearman correlation between count PCs and factors of unwanted variation (preferably derived from other set of negative control genes than used in RUV).
* **EXP_WV_COR**: Maximum squared Spearman correlation between count PCs and factors of wanted variation (derived from positive control genes).
* **RLE_MED**: Mean squared median relative log expression (RLE).
* **RLE_IQR**: Mean inter-quartile range (IQR) of RLE.

```{r include=FALSE}
load("../jsm2016/analysis/scone_res.rda")
scores <- scone_res$scores[,-NCOL(scone_res$scores)]
score_pca <- prcomp(scores)

lam <- score_pca$sdev[1:2] * sqrt(NROW(score_pca$x))
w <- rowMeans(t(t(score_pca$rotation[, 1:2]) * lam)^2)
```

## Exploring scone results

```{r scone_res}
biplot_colored(score_pca, -scone_res$scores[,"mean_score"])
```

```{r scone_res2, eval=FALSE}
apply(scores, 2, function(x) cor(x, score_pca$x[,1]))
apply(scores, 2, function(x) cor(x, score_pca$x[,2]))
```

Points color-coded by average score.

```{r norm_scone}
macklis <- read.table("../jsm2016/analysis/macklis_markers.txt")

filter <- filterCount(mat, 40, 10)
mat <- mat[filter,]

de <- intersect(macklis[,1], rownames(mat))
hk <- intersect(hk, rownames(mat))

params <- scone(mat, scaling = list(none=identity, TMM=TMM_FN, FQ=FQT_FN), adjust_bio = "yes", adjust_batch = "yes", bio = layer, batch = batch, ruv_negcon = hk, eval_poscon = de, qc = qc, run=FALSE)
norm <- scone(mat, scaling = list(none=identity, TMM=TMM_FN, FQ=FQT_FN), adjust_bio = "yes", adjust_batch = "yes", bio = layer, batch = batch, ruv_negcon = hk, eval_poscon = de, qc = qc, return_norm = "in_memory", evaluate=FALSE, params=params["none,none,ruv_k=4,bio,batch",])

norm_pca <- prcomp(t(norm$norm[[1]]))
```

```{r limma_tpm}
tpms <- assay(l456[rownames(mat),colnames(mat)], 3)
vars <- apply(log1p(tpms), 1, var)
vars <- sort(vars, decreasing=TRUE)
```

```{r limma_scone, dependson="norm_scone"}
vars2 <- apply(norm$norm[[1]], 1, var)
vars2 <- sort(vars2, decreasing=TRUE)
```

## TPM normalization

Heatmap of the 100 most variable genes

```{r, dependson="limma_tpm"}
tmp <- log1p(tpms[names(vars)[1:100],])
rownames(tmp) <- rep("", NROW(tmp))
NMF::aheatmap(tmp, annCol = data.frame(layer=layer), annColors=list(layer=collayer), color = seqPal3)
```

## RUV (k=4) + nested batch

Heatmap of the 100 most variable genes

```{r, dependson="limma_scone"}
tmp <- log1p(tpms[names(vars2)[1:100],])
rownames(tmp) <- rep("", NROW(tmp))
NMF::aheatmap(tmp, annCol = data.frame(layer=layer), annColors=list(layer=collayer), color = seqPal3)
```

```{r, include=FALSE}
pairs(pca$x[,1:3], pch=19, col=collayer[layer], main="TPM")
pairs(norm_pca$x[,1:3], pch=19, col=collayer[layer], main="QC (k=2) + nested batch")
```

# Clustering: resampling and sequential strategies

## Clustering of single-cell RNA-seq data

In the literature, most approaches can be summarized by three steps.

1. Dimensionality reduction (e.g., PCA, t-SNE, most variable genes).
2. Compute a distance matrix between samples in the reduce space.
3. Clustering based on a partitioning method (e.g., PAM, k-means).

\

For each step there are many tuning parameters. E.g.,

* How many principal components?
* Which distance? 
* How many clusters?

## Resampling-based Sequential Ensemble Clustering (RSEC)

Given a base cluster algorithm

* Generate a single candidate clustering using
    - **resampling** (to find robust clusters)
    - **sequential clustering** (to find stable clusters)
* Repeat the procedure for different algorithms and tuning parameters to generate a **collection of candidate clusterings**
* Identify a **consensus** over the different candidates

Implemented in the R/Bioconductor package **clusterExperiment**:
http://bioconductor.org/packages/clusterExperiment

\

<sub><sup>
Elizabeth Purdom
</sub></sup>

## Subsampling

Given an underlying clustering strategy, e.g., k-means or PAM with a particular choice of k, we repeat the following:

* Subsample the data, e.g. 70% of samples.
* Find clusters on the subsample.
* Create a co-clustering matrix D: 
    - % of subsamples where samples were in the same cluster.

## Sequential clustering

\

Our sequential clustering works as follows.

* Range over k in PAM clustering using the subsampling strategy.
* The cluster that remains stable across values of k is identified and removed.
* Repeat until no more stable clusters are found.

\
\
\

<sub><sup>
Inspired by the "tight clustering" algorithm \
Tseng and Wong (2005) 
http://dx.doi.org/10.1111/j.0006-341X.2005.031032.x
</sub></sup>

## Clustering reveals L5 sub-populations

```{r clustering}
library(clusterExperiment)
options(getClass.msg=FALSE)
load("~/git/brainData/analysis/results_160705/L5_clusterMany_res.rda")
options(expressions=1e5)

## combine clusters to get consensus clustering
cl <- combineMany(cl, minSize=10, proportion=0.5)

## get most variable genes for merging clusters
final_tight <- primaryCluster(cl)
wh_rm <- which(final_tight == -1)
vars <- apply(transform(cl)[,-wh_rm], 1, var)
vars <- sort(vars, decreasing=TRUE)

cl <- makeDendrogram(cl, dimReduce="var", ndims=1000, unassignedSamples="outgroup",
                     ignoreUnassignedVar = TRUE)

## merge clusters
cl <- mergeClusters(cl, isCount=TRUE, mergeMethod = "locfdr", plotType="none", cutoff=0.05)
```


```{r cl_plot, fig.height=6}
clusterLegend(cl)[[1]][,2] <- c("steelblue4", "violetred1", "orchid4", "darkseagreen2", "green1", "white", "dodgerblue2", "darkgrey", "tan1")
plotCoClustering(cl)
```

## Differential expression

* Find cluster gene expression **signatures** (marker genes).
* Standard solutions: 
    - F-test for any difference between clusters
    - all pairwise comparisons 
* Our solution:
    - Create a hierarchy of clusters
    - Select appropriate contrasts that compare sister nodes
  
## Differential expression

```{r ftest, fig.height=6}
cl <- makeDendrogram(cl, dimReduce="var", ndims=1000)
top50 <- getBestFeatures(cl, contrastType="F", isCount=TRUE, number=650)

plotHeatmap(cl, clusterFeaturesData=unique(top50$Feature), clusterSamplesData = "dendrogramValue", breaks=.95, main="F statistics")
```

## Differential expression

```{r dendro, fig.height=6}
top50 <- getBestFeatures(cl, contrastType="Dendro", isCount=TRUE, number=50)

plotHeatmap(cl, clusterFeaturesData=unique(top50$Feature), clusterSamplesData = "dendrogramValue", breaks=.95, main="Dendrogram contrasts")
```

## clusterExperiment R package

Functions to

* Generate a collection of candidate cluster labels.
* Find a consensus clustering.
* Merge clusters.
* Find cluster signatures.
* Visualize clustering results.

Available at http://bioconductor.org/packages/clusterExperiment

## clusterExperiment shiny app (coming soon!)

<div class="centered">
<img src="../common_figures/cluster_shiny.png" width="90%"/>
</div>

<sub><sup>
Liam Purvis and Elizabeth Purdom
</sub></sup>

## Summary {.emphasized}

* Sample quality influences (single-cell) RNA-seq expression data.
* We propose a flexible linear model to account for known and unknown factors of unwanted variation.
* **scone** helps explore different normalization schemes and rank them according to performance scores.
* Resampling-based sequential clustering strategies can help achieve stable and robust clusters.
* **clusterExperiment** provides a framework for comparing and visualizing different clustering techniques.

## Acknowledgements

<div class="centered">
<img src="../common_figures/acknowledgments2.png" width="100%"/>
</div>

## References {.emphasized}

* **RUVSeq R package**: http://bioconductor.org/packages/RUVSeq/
* **RUVSeq paper**: http://rdcu.be/jrCZ

\


* **scone**: https://github.com/YosefLab/scone
* **clusterExperiment**: http://bioconductor.org/packages/clusterExperiment
* **tutorial**: https://github.com/drisso/bioc2016singlecell

\


* **These slides**: http://rpubs.com/daviderisso/iisa2016
