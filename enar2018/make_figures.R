library(zinbwave)
library(ggplot2)
# library(cowplot)
theme_set(theme_classic())
library(scater)
library(irlba)
library(Rtsne)
library(magrittr)
library(RColorBrewer)

rm(list=ls())
set.seed(15938)

if(!dir.exists("plots")) dir.create("plots")

load("combined_zinbwave_all_batches.rda")
load("combined_filtered_all_batches.rda")

assays(filtered) <- list(counts = assay(filtered))
filtered <- normalize(as(filtered, "SingleCellExperiment"))

pca <- prcomp_irlba(t(logcounts(filtered)), scale = TRUE, n = 50)
tsne_data <- Rtsne(pca$x[,1:50], pca=FALSE, max_iter = 5000)

# dimReduced(filtered, "PCA") <- pca$x
# dimReduced(filtered, "TSNE") <- pca$x
# dimReduced(filtered, "ZINBWAVE") <- pca$x
# dimReduced(filtered, "TSNE_ZINB") <- pca$x

fig_data <- data.frame(pca$x, TSNE1 = tsne_data$Y[,1], TSNE2 = tsne_data$Y[,2],
                       colData(filtered))

ggplot(fig_data, aes(x = PC1, y = PC2, color = log10_total_features)) +
  geom_point(size = .7) + scale_color_continuous(low = "blue", high = "yellow") +
  theme(legend.title = element_text(size=8)) -> pca1

# ggsave("plots/pca1.png", pca1, height = 3, width = 5, dpi = 300, scale = 2)

pal <- c(brewer.pal(3, "Blues")[2:3], brewer.pal(5, "Purples")[2:5], brewer.pal(3, "Reds")[2:3])
qc <- as.matrix(colData(filtered)[,c(7, 9:13)])
ribo_idx <- grep("^Rpl", rowData(filtered)[,2])
mito_idx <- grep("^Mt", rowData(filtered)[,2])
ribo_pct <- colSums(assay(filtered)[ribo_idx,])/colSums(assay(filtered)) * 100
mito_pct <- colSums(assay(filtered)[mito_idx,])/colSums(assay(filtered)) * 100
qc <- cbind(qc, mito_pct, ribo_pct)

cors <- lapply(1:3, function(i) abs(cor(pca$x[,i], qc, method="spearman")))
cors <- unlist(cors)
bars <- data.frame(AbsoluteCorrelation=cors,
                   QC=factor(rep(colnames(qc), 3), levels=colnames(qc)),
                   Dimension=as.factor(rep(paste0("PC", 1:3), each=ncol(qc))))

bars %>%
  ggplot(aes(Dimension, AbsoluteCorrelation, group=QC, fill=QC)) +
  geom_bar(stat="identity", position='dodge') +
  scale_fill_manual(values = pal) + ylim(0, .8) +
  theme(legend.text=element_text(size=6)) -> pca2

# save_plot("plots/pca2.pdf", pca2)

ggplot(fig_data, aes(x = TSNE1, y = TSNE2, color = Batch)) +
  geom_point(size = .7) + scale_color_brewer(palette = "Set1") -> pca3

# save_plot("plots/pca3.pdf", pca3, base_height = 3, base_width = 5)

save(pca1, pca2, pca3, pal, file="plots/pca.rda")

## ZINB

set.seed(15938)
tsne_zinb <- Rtsne(getW(zinb), pca = FALSE, max_iter=5000)

W <- getW(zinb)
rownames(W) <- colnames(filtered)
colnames(W) <- paste0("W", 1:10)

cors <- lapply(1:3, function(i) abs(cor(W[,i], qc, method="spearman")))
cors <- unlist(cors)
bars <- data.frame(AbsoluteCorrelation=cors,
                   QC=factor(rep(colnames(qc), 3), levels=colnames(qc)),
                   Dimension=as.factor(rep(paste0("W", 1:3), each=ncol(qc))))

bars %>%
  ggplot(aes(Dimension, AbsoluteCorrelation, group=QC, fill=QC)) +
  geom_bar(stat="identity", position='dodge') +
  scale_fill_manual(values=pal) + ylim(0, .8) +
  theme(legend.text=element_text(size=6)) -> zinb1


fig_data <- data.frame(W, colData(filtered),
                       TSNE1=tsne_zinb$Y[,1], TSNE2=tsne_zinb$Y[,2])

ggplot(fig_data, aes(x = TSNE1, y = TSNE2, color = Batch)) +
  geom_point(size = .7) + scale_color_brewer(palette = "Set1") -> zinb2


load("clusterexperiment_all_batches.rda")

cols1 <- clusterLegend(cl2)[[1]][, "color"]
names(cols1) <- clusterLegend(cl2)[[1]][, "name"]

fig_data$Cluster <- primaryClusterNamed(cl2)
ggplot(fig_data, aes(x = TSNE1, y = TSNE2, color = Cluster)) +
  geom_point(size = .7) + scale_color_manual(values = cols1) -> zinb3

save(zinb1, zinb2, zinb3, cols1, file="plots/zinb.rda")

genes_1 <- getBestFeatures(cl2, contrastType = "OneAgainstAll", isCount=FALSE,
                           number=1)

library(pheatmap)
means <- t(apply(log1p(assay(cl2))[genes_1$Feature,], 1, tapply, primaryClusterNamed(cl2), mean))
df <-  data.frame(cluster=paste0("S", 0:14))
rownames(df) <- df[,1]

means <- means[,clusterLegend(cl2)[[1]][,"name"]]
pheatmap(means, color = colorRampPalette(clusterExperiment::seqPal5)(100), scale = "none", border_color = NA, annotation_col = df, annotation_colors = list(cluster = cols1), show_rownames = TRUE, annotation_legend = FALSE, cluster_cols = as.hclust(cl2@dendro_clusters))

pheatmap(means, color = colorRampPalette(clusterExperiment::seqPal3)(100), scale = "row", border_color = NA, annotation_col = df, annotation_colors = list(cluster = cols1), show_rownames = FALSE, annotation_legend = FALSE, cluster_cols = as.hclust(cl2@dendro_clusters))
