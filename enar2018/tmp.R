library(scRNAseq)




library(SingleCellExperiment)
library(scater)
library(scran)
library(zinbwave)
library(BiocParallel)
library(ClusterExperiment)
library(DESeq2)

register(SerialParam())

data(allen)
allen

sce <- as(allen, "SingleCellExperiment")
isSpike(sce, "ERCC") <- grepl("^ERCC-", rownames(sce))
assayNames(sce)[1] <- "counts"
sce

# Simple subsetting
sce <- sce[1:1000, 1:100]
sce <- sce[rowSums(counts(sce))>0, !is.na(sce$Primary.Type)]


# QC/EDA with scater
sce <- calculateQCMetrics(sce)

# Normalization with scran
sce <- computeSumFactors(sce)
sce <- normalize(sce)

# Dimensionality reduction with zinbwave
sce <- zinbwave(sce, K = 2)

# Clustering with clusterExperiment
sce <- clusterSingle(sce, reduceMethod = "zinbwave", nDims = 2,
                     subsample = FALSE,
                     mainClusterArgs = list(clusterFunction = "kmeans",
                                          clusterArgs = list(k = 3)))

# Differential expression with DESeq2
dds <- DESeqDataSet(sce, design = ~ Primary.Type)
dds <- DESeq(dds, sfType="poscounts", useT=TRUE, minmu=1e-6)
res <- results(dds)


# library(Seurat)
#
# ## Subsetting a Seurat object
# seu <- CreateSeuratObject(raw.data = counts(sce))
# seu <- NormalizeData(object = seu, normalization.method = "LogNormalize",
#                       scale.factor = 10000)
# seu <- FindVariableGenes(object = seu, mean.function = ExpMean, dispersion.function = LogVMR,
#                           x.low.cutoff = 0.0125, x.high.cutoff = 3, y.cutoff = 0.5)
# seu <- ScaleData(object = seu)
# seu <- RunPCA(object = seu, pc.genes = seu@var.genes, do.print = TRUE, pcs.print = 1:5,
#                genes.print = 5)
# seu <- FindClusters(object = seu, reduction.type = "pca", dims.use = 1:10,
#                      resolution = 0.6, print.output = 0, save.SNN = TRUE)
# seu <- RunTSNE(object = seu, dims.use = 1:10, do.fast = TRUE)

