---
title: "Some tools for scRNA-seq analysis"
author: "Davide Risso"
date: "7/17/2017"
output: 
  ioslides_presentation:
    css: my-css.css 
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## Single-cell signal is low-dimensional

<center>
<img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/cell_state.png" style="width: 540px;"/>
</center>

<div style="text-align: right">
<font size="3">Wagner, Regev, Yosef (2016)</font>
</div>

## Single-cell signal is noisy {.build}

- High level of technical and biological variance.
- High drop-out rate (zero inflation).
- Amplification bias (high expression outliers).
- Unique Molecular Identifiers (UMIs) help but do not solve all these problems.

### Problematic with typical workflow: global scaling + log + PCA

## A typical scRNA-seq workflow

![](img/workflow.png)

## scone: QC, filtering, and normalization

![](img/scone0.png)

## 

![](img/scone1.jpg)

## 

![](img/scone2.jpg)

## 

![](img/scone3.jpg)

## 

![](img/scone4.jpg)

##

![](img/scone5.jpg)

## ZINB-WaVE: dimensionality reduction

![](https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/biorxiv.png)

## 

![](img/zinbwave1.png)

## The negative binomial distribution

For any $\mu\geq 0$ and $\theta>0$, the probability mass function
(PMF) of the negative binomial (NB) distribution is 

$$
f_N(y;\mu,\theta) = \frac{\Gamma(y+\theta)}{\Gamma(y+1)\Gamma(\theta)} \left(\frac{\theta}{\theta+\mu}\right)^{\theta} \left(\frac{\mu}{\mu + \theta}\right)^y, \quad \forall y\in\mathbb{N}.
$$


The mean of the NB distribution is $\mu$ and its variance is:
$$
\sigma^2 = \mu + \frac{\mu^2}{\theta}.
$$
In particular, the NB distribution boils down to a Poisson
distribution when $\theta = +\infty$.

## The zero-inflated negative binomial

For any $\pi\in[0,1]$, 
the PMF of the zero-inflated negative binomial (ZINB) distribution is
given by


$$
f(y;\mu,\theta, \pi) = \pi \delta_0(y) + (1 - \pi) f_N(y;\mu,\theta), \quad \forall y\in\mathbb{N},
$$

where $\delta_0(\cdot)$ is the Dirac function. 

Here, $\pi$ can be interpreted as the probability that a $0$ is observed instead of the actual count, resulting in an inflation of zeros compared to the NB distribution, hence the name ZINB.

## The ZINB-WaVE model

Given $n$ samples and $J$ genes, let
$Y_{ij}$ denote the count of gene $j$ (for $j=1,\ldots,J$) for
sample $i$ (for $i=1,\ldots,n$). 

<img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/zinb_schema.jpg" style="width: 800px;"/>

## 10x 4k PBMCs 

<center>
<img src="img/zinbwave2.png" style="width: 540px;"/>
</center>

## 10x 4k PBMCs {.small}

<center>
<img src="img/zinbwave9.png" style="width: 500px;"/>
</center>

Able to find a set of Plasmacytoid dendritic cells (pDCs) that constitute < 0.4% of PBMC. 

## clusterExperiment

![](img/cluster0.png)

##

![](img/cluster1.jpg)

##

![](img/cluster2.jpg)

##

![](img/cluster3.jpg)

##

![](img/cluster4.jpg)

##

![](img/cluster5.jpg)

##

![](img/cluster6.jpg)

##

![](img/cluster7.jpg)

##

![](img/cluster8.jpg)

##

![](img/cluster9.jpg)

## Validation

<center>
<img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/validation.png" style="width: 600px;"/>
</center>

## slingshot: lineage inference

![](img/sling0.png)

## The slingshot algorithm

![](img/sling1.png)

## Robustness to noise

![](img/sling2.png)

## Robustness to misspecification

![](img/sling3.png)

## Comparison with existing tools

![](img/sling4.png)

## Linking all together

![](img/workflow1.png)

## Linking all together

![](img/workflow2.png)

## Challenge: scale up to millions of cells

- Tools are scalable to thousands of cells.
- 13 CPU hours for analysis of 68k PBMCs from 10X Genomics.
- 10X Genomics released 1.3 Million cells dataset!
- Main problem: does not fit in memory!
- Partial solution: HD5 files + "chunk operations"
- Simple algorithms + approximate, scalable methods

## Bioconductor relevant infrastructure

![](img/bioc1.png)

## Bioconductor relevant infrastructure

![](img/bioc2.png)

## Bioconductor relevant infrastructure

![](img/bioc3.png)

## Bioconductor relevant infrastructure

![](img/bioc4.png)

## Bioconductor relevant infrastructure

![](img/bioc5.png)
