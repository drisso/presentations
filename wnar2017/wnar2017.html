<!DOCTYPE html>
<html>
  <head>
    <title>ZINB-WaVE</title>
    <meta charset="utf-8">
    <meta name="author" content="Davide Risso" />
    <meta name="date" content="2017-06-27" />
    <link href="libs/remark-css/example.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-theme.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# ZINB-WaVE
## A General and Flexible Method for Signal Extraction from Single-Cell RNA-Seq Data
### Davide Risso
### June 27, 2017

---




![](https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/biorxiv.png)

.center[[biorxiv.org/content/early/2017/04/06/125112](http://biorxiv.org/content/early/2017/04/06/125112)]

.center[Bioconductor package]

.center[[bioconductor.org/packages/zinbwave](https://bioconductor.org/packages/zinbwave)]


---

# Outline

- Motivation

- The ZINB-WaVE model

- Estimation procedure

- Comparison with existing approaches


---
class: center, middle

# Motivation

---
class: center

## Single-cell signal is intrinsecally low-dimensional

&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/cell_state.png" style="width: 550px;"/&gt;

.right[.small[Wagner, Regev, Yosef (2016)]]

---

# Single-cell signal is noisy

&lt;br&gt;&lt;br&gt;

- High level of technical and biological variance.

- High drop-out rate (zero inflation).

- Amplification bias (high expression outliers).

- Unique Molecular Identifiers (UMIs) help but do not solve all these problems.

&lt;br&gt;&lt;br&gt;&lt;br&gt;

### .center[.red[Problematic with typical workflow: global scaling + log + PCA]]
---

# Characterization of Layer 5 cortical neurons



&lt;img src="wnar2017_files/figure-html/pca-1.png" width="864" style="display: block; margin: auto;" /&gt;

---

# PCA is affected by sample quality

&lt;img src="wnar2017_files/figure-html/pca2-1.png" width="864" style="display: block; margin: auto;" /&gt;

---
class: center, middle

# The ZINB-WaVE model

---

# The ZINB-WaVE model

- General and flexible a zero-inflated negative binomial (ZINB) model.

- Extract low-dimensional signal from the data.

- Accounting for zero inflation (dropouts), over-dispersion, and the count nature of the data.

--

- The model includes a sample-level intercept, which serves as a global-scaling normalization factor.

- Gives the user the ability to include both gene-level and sample-level covariates.

--

- Inclusion of observed and unobserved sample-level covariates enables normalization for complex, non-linear effects (batch effects)

- Gene-level covariates may be used to adjust for sequence composition effects, such as gene length and GC-content effects.

---

# The negative binomial distribution

For any `\(\mu\geq 0\)` and `\(\theta&gt;0\)`, the probability mass function
(PMF) of the negative binomial (NB) distribution is 

$$
f_N(y;\mu,\theta) = \frac{\Gamma(y+\theta)}{\Gamma(y+1)\Gamma(\theta)} \left(\frac{\theta}{\theta+\mu}\right)^{\theta} \left(\frac{\mu}{\mu + \theta}\right)^y, \quad \forall y\in\mathbb{N}.
$$


The mean of the NB distribution is `\(\mu\)` and its variance is:
$$
\sigma^2 = \mu + \frac{\mu^2}{\theta}.
$$
In particular, the NB distribution boils down to a Poisson
distribution when `\(\theta = +\infty\)`.

---

# The zero-inflated negative binomial

&lt;br&gt;&lt;br&gt;

For any `\(\pi\in[0,1]\)`, 
the PMF of the zero-inflated negative binomial (ZINB) distribution is
given by


$$
f(y;\mu,\theta, \pi) = \pi \delta_0(y) + (1 - \pi) f_N(y;\mu,\theta), \quad \forall y\in\mathbb{N},
$$

where `\(\delta_0(\cdot)\)` is the Dirac function. 

&lt;br&gt;&lt;br&gt;

Here, `\(\pi\)` can be interpreted as the probability that a `\(0\)` is observed instead of the actual count, resulting in an inflation of zeros compared to the NB distribution, hence the name ZINB.

---

# The ZINB-WaVE model

Given `\(n\)` samples and `\(J\)` genes, let
`\(Y_{ij}\)` denote the count of gene `\(j\)` (for `\(j=1,\ldots,J\)`) for
sample `\(i\)` (for `\(i=1,\ldots,n\)`). 

--

&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/zinb_schema.jpg" style="width: 800px;"/&gt;

---

# The ZINB-WaVE model

- The `\(W \alpha\)` term refers to unknown low-dimensional variation, that could be due to unwanted technical effects, such as batch effects, or to biological effects of interest, such as cell cycle or cell differentiation.

--

-  `\(W\)` is the same, but `\(X\)` and `\(V\)` could differ in the modeling of `\(\mu\)` and `\(\pi\)`, if we assume that some known factors do not affect both. 

--

- When `\(X = 1_n\)` and `\(V = 1_J\)`, the model is a factor model akin to PCA, where `\(W\)` is a factor matrix and `\(\alpha_\mu\)` and `\(\alpha_\pi\)` are loading matrices.

--

- Note that the intercept in `\(V\)` acts as a scaling factor, hence the model can be applied to raw counts (no need for prior normalization).

--

- The model is more general, allowing the inclusion of additional sample and gene-level covariates that might help to infer the unknown factors.

--

- The model includes a gene-wise dispersion parameter, but in practice a single common dispersion works well.

---

# Estimation procedure

To estimate the parameters `\((\alpha, \beta, \gamma, W, \theta)\)`, we follow a penalized maximum likelihood approach, to reduce
overfitting and improve the numerical stability of the optimization
problem in the setting of many parameters.

![](https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/likelihood.png)

where `\(\zeta = \log \theta\)` and

![](https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/penalty.png)

---

# Estimation procedure

- `\(\beta^0\)` and `\(\gamma^0\)` denote the matrices `\(\beta\)` and
`\(\gamma\)` without the rows corresponding to the intercepts if an
unpenalized intercept is included in the model.

- `\(\|\cdot\|\)` is the Frobenius matrix norm ( `\(\|A\| = \sqrt{\text{tr}(A^*A)}\)`, where `\(A^*\)` denotes the conjugate transpose of `\(A\)`).

- The penalty shrinks the estimated
parameters to `\(0\)`, except for the cell and gene-specific intercepts which are not penalized and the dispersion parameters which are shrunk towards a constant value across genes (cf. edgeR).

- The penalty also ensures that at the optimum `\(W\)` and `\(\alpha^T\)` have orthogonal columns, which is useful for visualization or interpretation of latent factors.

---

# Optimization procedure

The penalized likelihood is not concave, making its maximization computationally challenging. 

&lt;br&gt;&lt;br&gt;

We instead find a local maximum, starting from a smart initialization and iterating a numerical optimization scheme until local convergence.

---

1. Dispersion optimization
     ![](img/dispersion_est.png)

2. Left factor (cell-specific) optimization

     ![](img/left_factors.png)

3. Right factor (sample-specific) optimization

     ![](img/right_factors.png)

4. Orthogonalization

     ![](img/orthogonal.png)
---
class: center, middle

# Comparison with existing approaches

---
class: center

# Simulated data (correlation)

&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/boxplots.png" style="width: 500px;"/&gt;
&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/legend.png" style="width: 200px;"/&gt;

---
class: center

# Simulated data (silhouette width)

&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/sil.png" style="width: 450px;"/&gt;
&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/legend.png" style="width: 200px;"/&gt;

---
class: center

# Real data (silhouette width)

&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/realdata.jpg" style="width: 650px;"/&gt;

---
class: center, middle

# Back to layer 5 neurons...

---

# PCA is affected by sample quality

&lt;img src="wnar2017_files/figure-html/pca3-1.png" width="864" style="display: block; margin: auto;" /&gt;

---

# ZINB-WaVE adjusts for QC


```r
qcpca &lt;- prcomp(qc, scale. = TRUE)
```


```r
qcmod &lt;- model.matrix(~qcpca$x[,1:2])
head(qcmod)
```

```
##   (Intercept) qcpca$x[, 1:2]PC1 qcpca$x[, 1:2]PC2
## 1           1        -2.0656901        0.53766716
## 2           1         1.8332237       -0.11599891
## 3           1         1.2944881       -0.03171802
## 4           1         0.5874294        0.87552522
## 5           1        -2.0671591        1.12292102
## 6           1        -2.1200918        0.94022293
```


```r
zinbres &lt;- zinbFit(filtered, K = 3, X = qcmod)
```

---

# ZINB-WaVE adjusts for QC

&lt;img src="wnar2017_files/figure-html/zinb-1.png" width="864" style="display: block; margin: auto;" /&gt;

---

### ZINB-WaVE yields biologically meaningful signal

&lt;img src="wnar2017_files/figure-html/zinb2-1.png" width="864" style="display: block; margin: auto;" /&gt;

---

### ZINB-WaVE yields biologically meaningful signal

&lt;img src="wnar2017_files/figure-html/zinb3-1.png" width="864" style="display: block; margin: auto;" /&gt;

---

### ZINB-WaVE yields biologically meaningful signal

&lt;img src="https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/validation.png" style="width: 650px;"/&gt;

---

# Conclusions and future directions

- ZINB-WaVE is a general and flexible method for low-dimensional signal extraction from noisy, zero-inflated count data.

--

- It works well for projecting single-cell data in two dimensions as a dimensionality reduction step prior to clustering or pseudotime inference.

--

- We are extending the model to provide normalized values for visualization (residuals).

--

- And to supervised differential expression problems (test `\(\beta = 0\)`).

---
.pull-right[
# Acknowledgements
]


### University of California, Berkeley

Fanny Perradeau, Sandrine Dudoit

John Ngai, David Stafford

.pull-right[
### Institut Curie, Paris, France

Svetlana Gribkova

Jean-Philippe Vert
]

---
class: center, middle

# Thank you!

.pull-left[
ZINB-Wave preprint: 

[doi.org/10.1101/125112](https://doi.org/10.1101/125112)

`zinbwave` package: 

[bioconductor.org/packages/zinbwave](https://bioconductor.org/packages/zinbwave)

.left[
Contacts:

Github: [@drisso](https://github.com/drisso)

Twitter: [@drisso1893](https://twitter.com)

Email: [`dar2062@med.cornell.edu`](mailto:dar2062@med.cornell.edu)
]
]

.pull-right[
![zinbsticker](https://gitlab.com/drisso/presentations/raw/master/wnar2017/img/zinbwave.png)
]
    </textarea>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {window.dispatchEvent(new Event('resize'));});</script>

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: {
    skipTags: ['script', 'noscript', 'style', 'textarea', 'pre']
  }
});
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://cdn.bootcss.com/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
