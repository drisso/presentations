---
title: "A zero-inflated negative binomial model for single-cell RNA-seq"
author: "Davide Risso"
date: "Joint work with:\\ Svetlana Gribkova, Fanny Perraudeau, Sandrine Dudoit, JP Vert"
output: 
    beamer_presentation:
      incremental: yes
      slide_level: 3
      toc: no
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(cache=TRUE, message=FALSE, warning=FALSE, echo=FALSE, results="hide", out.width="\\linewidth")
```

# Introduction

### Single-cell RNA-seq

\centering
\includegraphics[width=\textwidth]{../common_figures/bulk_single}	


### Negative binomial

RNA-seq data are often modeled by a **negative binomial** distribution.
 
\begin{align*}
E[Y_{ij}] &= \mu_{ij},\\
Var(Y_{ij}) &= \mu_{ij} + \phi_j \, \mu_{ij}^2.
\end{align*}

Modeling is done via Generalized Linear Models (GLMs) (one regression per gene):

\begin{equation*}
\log E[Y_{ij} | X_i = x_i] = \beta_{0j} + \beta_{1j} \, x_i;
\end{equation*}

if two-class comparison, $\beta_{1j}$ is the log-ratio of the mean of the two groups (\emph{log-fold-change}).

\bigskip
Likelihood ratio tests are used to test $H_0: \beta_{1j} = 0$ (\emph{differential expression}).

###

\centering
\includegraphics[width=\textwidth]{figures/Slide1.jpg}

###

\centering
\includegraphics[width=\textwidth]{figures/Slide2.jpg}

###

\centering
\includegraphics[width=\textwidth]{figures/Slide3.jpg}

### Exponential decay fit

\centering
\includegraphics[width=\textwidth]{figures/pierson4.png}


### Why do we need a new model?

- kharchenko et al. (2014) and Finak et al. (2015) are designed for _supervised problems_, e.g., differential expression.
- Pierson and Yau (2015) is a factor analysis model, used to extract a low-dimensional signal from the count matrix.
- kharchenko et al. (2014) is limited to two-class comparisons.
- Pierson and Yau (2015) does not allow for covariates.
- Finak et al. (2015) allows for sample-level covariates.
- No model allows the use of gene-level covariates (such as gene length and GC-content).
- Finak et al. (2015) and Pierson and Yau (2015) ignore the count nature of the data (e.g., mean-variance relation).

### Normalization

- kharchenko et al. (2014) and Pierson and Yau (2015) do not include normalization terms, assuming one starts with normalized data.
- Finak et al. (2015) use the _cellular detection rate_ (CDR) as a covariate to account for normalization:
$$
CDR_i = 1/N \sum_{g=1}^N z_{ig},
$$
where $z_{ig}$ is an indicator that gene $g$ is expressed in cell $i$ _above background_.

### A zero-inflated negative binomial model

- Given that RNA-seq data have often been modeled with negative binomials, it seems natural to use a zero-inflated negative binomial (ZINB) to model the extra zeros.
- We propose a general and flexible ZINB model that can have:
    - gene-level and sample-level covariates
    - a gene-level intercept that acts as a normalization term
    - both supervised and unsupervised applications


# The model

### Negative binomial

For any $\mu\geq 0$ and $\theta>0$, let $f_{NB}( \cdot \,;\mu,\theta)$ denote the probability mass function (p.m.f.) of the negative binomial (NB) distribution with mean $\mu$ and inverse dispersion parameter $\theta$, namely:
$$
\forall y\in\mathbb{N},\quad f_{NB}(y;\mu,\theta) = \frac{\Gamma(y+\theta)}{\Gamma(y+1)\Gamma(\theta)} \left(\frac{\theta}{\theta+\mu}\right)^{\theta} \left(\frac{\mu}{\mu + \theta}\right)^y \,.
$$

### Zero-inflated negative binomial

For any $\pi\in[0,1]$, let $f_{ZINB}( \cdot \,;\mu,\theta, \pi)$ be the p.m.f. of the zero-inflated negative binomial (ZINB) distribution given by:
$$
\forall y\in\mathbb{N},\quad f_{ZINB}(y;\mu,\theta, \pi) = \pi \delta_0(y) + (1-\pi) f_{NB}(y;\mu,\theta) \,,
$$
where $\delta_0(\cdot)$ is the Dirac function. Here, $\pi$ can be interpreted as the probability that a $0$ is observed instead of the actual count, resulting in an inflation of zeros compared to the NB distribution, hence the name ZINB.

### A regression model

Given $n$ samples (typically, $n$ single cells) and $J$ features (typically, $J$ genes), $Y_{ij}$ is a random variable following a ZINB distribution with parameters $\mu_{ij}, \theta_{ij}$, and $\pi_{ij}$, and consider the following model for the parameters:
\begin{align}
\label{eq:model1}
\ln(\mu_{ij}) &= \left( X\beta_\mu + (V\gamma_\mu)^\top + W\alpha_\mu + O_\mu\right)_{ij}\,,\\
\label{eq:model2}
\text{logit}(\pi_{ij}) &= \left(X\beta_\pi + (V\gamma_\pi)^\top + W\alpha_\pi + O_\pi\right)_{ij} \,,\\
\label{eq:model3}
\ln(\theta_{ij}) &= \zeta_j \,.
\end{align}

### A regression model

- $X$ is a known $n \times M$ design matrix corresponding to $M$ cell-level covariates and ${\bf \beta}=(\beta_\mu,\beta_\pi)$ its associated $M \times J$ matrices of regression parameters. $X$ can typically include covariates that induce variation of interest, such as cell types, or covariates that induce unwanted variation, such as batch or QC measures. It can also include a constant column ${\bf 1}_n$ to account for gene-specific intercepts.
- $V$ is a known $J \times L$ matrix corresponding to $J$ gene-level covariates, such as gene length or GC-content, and ${\bf \gamma} = (\gamma_\mu , \gamma_\pi)$ its associated $L\times n$ matrices of regression parameters. $V$ can also include a constant column ${\bf 1}_J$ to account for cell-specific intercepts, such as size factors representing differences in library sizes.

### A regression model

- $W$ is an unobserved $n \times K$ matrix corresponding to $K$ unknown cell-level covariates, which could be of “unwanted variation” as in RUV or of interest (such as cell type), and ${\bf \alpha} = (\alpha_\mu,\alpha_{\pi})$ its associated $K \times J$ matrices of regression parameters.
- $O_\mu$ and $O_\pi$ are known $n \times J$ matrices of offsets.
- $\zeta\in\mathbb{R}^J$ is a vector of gene-specific dispersion parameters on the log scale.

### A few comments

- The model extends the RUV framework to the ZINB distribution.
- However, $W\alpha$  is not necessarily considered unwanted; this term generally corresponds to unknown low-dimensional variation, that could be due to unwanted techical effects (as in RUV), such as batch effects, or to biological effects which could be of interest, such as cell cycle or cell differentiation, as typically assumed in other factor models such as principal component analysis (PCA) or independent component analysis (ICA).

### A few comments

- By default, $X$ and $V$ contain a constant column of ones, to account, respectively, for gene-specific (e.g., mean expression level) and cell-specific (e.g., size factors) variation.
- When $X={\bf 1}_n$ and $V={\bf 1}_J$, the model is a factor model akin to PCA, where $W$ is a factor matrix and $(\alpha_\mu, \alpha_\pi)$ are loading matrices.

### A few comments

- The $X$ and $V$ matrices could differ in the modelling of $\mu$ and $\pi$ if we assume that some known factors do not affect both $\mu$ and $\pi$; to keep notation simple and consistent we use the same matrices, but will implicitly assume that some parameters may be constrained to be $0$ if needed.
- By allowing the parameters to differ between the models of $\mu$ and $\pi$, we can model and test for differences in NB mean or in ZI probability.
- We limit ourselves to a gene-dependent dispersion parameter. More complicated models for $\theta_{ij}$ could be investigated, such as a model similar to $\mu_{ij}$ or a functional of the form $\theta_{ij} = f(\mu_{ij})$, but we restrict ourselves to a simpler model that has been shown to be largely sufficient in bulk RNA-Seq analysis.


## Parameter estimation

### Likelihood function

Given an $n\times J$ matrix of counts $Y$, the log-likelihood function is
$$
\ell(\beta,\gamma,W,\alpha,\zeta) = \sum_{i=1}^n \sum_{j=1}^J \ln f_{ZINB}(Y_{ij};\mu_{ij},\theta_{ij}, \pi_{ij}) \,,
$$
where $\mu_{ij}$, $\theta_{ij}$, and $\pi_{ij}$ depend on the unknown parameters $(\beta,\gamma,W,\alpha,\zeta)$. 

### Penalized maximum likelihood

To infer the parameters, we follow a penalized maximum likelihood approach, by trying to solve
$$
\max_{\beta,\gamma,W,\alpha,\zeta} \left\{\ell(\beta,\gamma,W,\alpha,\zeta) - \text{Pen}(\beta,\gamma,W,\alpha,\zeta) \right\}\,,
$$
where $\text{Pen}(\cdot)$ is a regularization term to reduce overfitting and improve the numerical stability of the optimization problem in the setting of many parameters.

### Penalized maximum likelihood

For a set of nonnegative regularization parameters $\left( \epsilon_\beta , \epsilon_\gamma, \epsilon_W, \epsilon_\alpha, \epsilon_\zeta \right)$, we set
\begin{small}
$$
\text{Pen}(\beta,\gamma,W,\alpha,\zeta) = \frac{\epsilon_{\beta}}{2} \|\beta^0\|^2 + \frac{\epsilon_{\gamma}}{2} \|\gamma^0\|^2 + \frac{\epsilon_{W}}{2}\|W\|^2 + \frac{\epsilon_{\alpha}}{2}\|\alpha\|^2 + \frac{\epsilon_{\zeta}}{2}\text{Var}(\zeta) \,,
$$
\end{small}
where $\beta^0$ and $\gamma^0$ denote the matrices $\beta$ and $\gamma$ without the rows corresponding to the intercepts if an unpenalized intercept is included in the model, $\|\cdot\|$ is the Frobenius matrix norm, and $\text{Var}(\zeta)$ is the variance of the elements of $\zeta$. 

### A few comments

- The penalty tends to shrink the estimated parameters to $0$, except for the cell- and gene-specific intercepts which are not penalized and the dispersion parameters which are not shrunk towards $0$ but instead towards a constant value across genes.
- The likelihood only depends on $W$ and $\alpha$ through their product $R=W\alpha$ and the penalty ensures that at the optimum $W$ and $\alpha$ are such that any local maximum of the penalized log-likelihood, $W$ and $\alpha^\top$ have orthogonal columns, which is useful for visualization or interpretation of latent factors.
- The penalized likelihood is not concave, making its maximization computationally challenging. We instead find a local maximum, starting from a smart initialization and iterating a numerical optimization scheme until local convergence.

### Initialization

To initialize the set of parameters we approximate the count distribution by a log-normal distribution and explicitly separate zero and non-zero values, as follows:


1. Set $\mathcal{P} = \left\{(i,j)\,:\,Y_{ij}>0\right\}$.
2. Set $L_{ij} = \ln(Y_{ij}) - (O_\mu)_{ij}$ for all $(i,j)\in\mathcal{P}$. 
3. Set $\hat{Z}_{ij} = 1$ if $(i,j)\in\mathcal{P}$, $\hat{Z}_{ij} =0$ otherwise.
4. Estimate $\beta_\mu$ and $\gamma_\mu$ by solving the convex ridge regression problem:
$$
\min_{\beta_\mu, \gamma_\mu} \sum_{(i,j)\in\mathcal{P}} \left(L_{ij} - (X\beta_{\mu})_{ij} -  (V\gamma_\mu)_{ji} \right)^2 +  \frac{\epsilon_\beta}{2} \|\beta_\mu^0\|^2 + \frac{\epsilon_\gamma}{2} \|\gamma_\mu^0\|^2 \,.
$$

### Initialization

This is a standard ridge regression problem, but with a potentially huge design matrix, with up to $nJ$ rows and $MJ+nL$ columns. To solve it efficiently, we alternate the estimation of $\beta_\mu$ and $\gamma_\mu$. Specifically, we initialize parameter values as:
$$
\hat{\beta}_\mu \leftarrow 0 \,,\quad\hat{\gamma}_\mu \leftarrow 0\, 
$$
and repeat the following two steps a few times (or until convergence):

1. Optimization in $\gamma_\mu$, which can be performed independently and in parallel for each cell:
$$
\hat{\gamma}_\mu \in \arg\min_{\gamma_\mu} \sum_{(i,j)\in\mathcal{P}} \left(L_{ij} - (X\hat{\beta}_{\mu})_{ij} - (V\gamma_\mu)_{ji} \right)^2 + \frac{\epsilon_\gamma}{2} \|\gamma_\mu^0\|^2\,.
$$
2. Optimization in $\beta_\mu$, which can be performed independently and in parallel for each gene:
$$
\hat{\beta}_\mu \in \arg\min_{\beta_\mu} \sum_{(i,j)\in\mathcal{P}} \left(L_{ij} - (V\hat{\gamma}_\mu)_{ji} - (X\beta_{\mu})_{ij} \right)^2 + \frac{\epsilon_\beta}{2} \|\beta_\mu^0\|^2\,.
$$

### Initialization

5. Estimate $W$ and $\alpha_\mu$ by solving

\begin{scriptsize}
$$
\left(\hat{W},\hat{\alpha}_\mu\right) \in \arg\min_{W,\alpha_\mu} \sum_{(i,j)\in\mathcal{P}} \left(L_{ij} - (X\hat{\beta}_{\mu})_{ij} - (V\hat{\gamma}_\mu)_{ji} - (W\alpha_\mu)_{ij} \right)^2 + \frac{\epsilon_W}{2} \|W\|^2 + \frac{\epsilon_\alpha}{2} \|\alpha_\mu\|^2\,.
$$
\end{scriptsize}

6. Estimate $\beta_\pi$, $\gamma_\pi$, and $\alpha_\pi$ by ridge logistic regression,with an alternating strategy similar to what done for $\beta_\mu$ and $\gamma_\mu$.
7. Initialize $\hat{\zeta}=0$.

### Optimization

1. Dispersion optimization:
$$
\hat{\zeta} \leftarrow \arg\max_{\zeta} \left\{\ell(\hat{\beta}, \hat{\gamma}, \hat{W}, \hat{\alpha}, \zeta) - \frac{\epsilon_\zeta}{2} \text{Var}(\zeta) \right\} \,.
$$
To solve this problem:

    1. Estimate a common dispersion parameter for all the genes, by maximizing the objective function under the constraint that $\text{Var}(\zeta)=0$;
    2. Optimize the objective function by a quasi-Newton optimization scheme starting from the constant solution found by the first step.

### Optimization

2. Left-factor (cell-specific) optimization:
\begin{scriptsize}
\begin{equation}\label{eq:leftoptim}
\left(\hat{\gamma}, \hat{W} \right) \leftarrow \arg\max_{\left(\gamma, W\right)} \left\{\ell(\hat{\beta}, \gamma, W, \hat{\alpha}, \hat{\zeta}) - \frac{\epsilon_\gamma}{2} \|\gamma^0\|^2 - \frac{\epsilon_W}{2} \|W\|^2 \right\}\,.
\end{equation}
\end{scriptsize}

Note that this optimization can be performed independently and in parallel for each cell $i=1,\ldots,n$.

### Optimization

3. Right-factor (gene-specific) optimization:
\begin{scriptsize}
$$
\left(\hat{\beta}, \hat{\alpha} \right) \leftarrow \arg\max_{\left(\beta, \alpha \right)} \left\{\ell(\beta, \hat{\gamma},\hat{W}, \alpha, \hat{\zeta}) - \frac{\epsilon_\beta}{2} \|\beta^0\|^2 - \frac{\epsilon_\alpha}{2} \|\alpha\|^2\right\}\,.
$$
\end{scriptsize}
Note that this optimization can be performed independently and in parallel for each gene $j=1,\ldots,J$.

### Optimization

4. Orthogonalization: 
\begin{scriptsize}
$$
\left(\hat{W}, \hat{\alpha} \right) \leftarrow \arg\min_{(W,\alpha)\,:\,W\alpha = \hat{W}\hat{\alpha}} \frac{1}{2} \left( \epsilon_W \|W\|^2 + \epsilon_\alpha \|\alpha\|^2 \right)\,.
$$
\end{scriptsize}
Note that this step not only allows to maximize locally the penalized log-likelihood, but also ensures that the columns of $W$ stay orthogonal to each other during optimization.

# Results

### Unsupervised setting

We test the model in an unsupervised setting.

We assume that the data can be explained by a low-dimensional signal and we try to infer such signal.

## Preliminary simulations (with Fanny)

### Simulation setting

We simulate from the proposed model, with the following parameters.

- 2 unknown factors in $W$
- $V$ intercept
- $X$ intercept
- Common dispersion

To have realistic values for the parameters, we estimated the values starting from the allen dataset, randomly selecting 100 cells and using only the top 1,000 most variable genes.

### Simulation setting

For each simulated data, we fit a ZINB model with all the possible combinations (32) of the following parameters:

- Dimension of $W$: 1--4
- $V$ intercept: yes, no
- $X$ intercept: yes, no
- Dispersion: common, genewise

This is obviously just one simulation setting, we will need to simulate data with genewise dispersion, no intercepts, etc.

### Explore the results for one simulated matrix

```{r one_sim}
library(cluster)
library(zinb)
load("~/git/zinb_analysis/sims/k2_Xintercept_Vintercept.rda")
load("~/git/zinb_analysis/sims/k2_Xintercept_Vintercept_first.rda")
true_W <- sim_obj@W
dtrue <- as.matrix(dist(true_W))
pamtrue <- pam(dtrue, k=3)$clustering

plot(true_W, pch=19, col=pamtrue, main="True W", xlab="W1", ylab="W2")
```

### Explore the results for one simulated matrix

```{r one_sim1}
plot(fit[[1]]@W, rep(0, NROW(true_W)), pch=19, col=pamtrue, main="K=1", xlab="W1", ylab="")
```

### Explore the results for one simulated matrix

```{r one_sim2}
plot(fit[[2]]@W, pch=19, col=pamtrue, main="K=2", xlab="W1", ylab="W2")
```

### Explore the results for one simulated matrix

```{r one_sim3}
pairs(fit[[3]]@W, pch=19, col=pamtrue, main="K=3")
```

### Explore the results for one simulated matrix

```{r one_sim4}
pairs(fit[[4]]@W, pch=19, col=pamtrue, main="K=4")
```

### Explore the results for one simulated matrix

```{r one_sim5}
pca <- prcomp(log1p(sim_data[[1]]$counts))
plot(pca$x, pch=19, col=pamtrue, main="PCA of unnormalized log-counts", xlab="PC1", ylab="PC2")
```

### Explore the results for one simulated matrix

```{r one_sim6}
library(EDASeq)
fq <- betweenLaneNormalization(t(sim_data[[1]]$counts), which="full")
pca <- prcomp(t(log1p(fq)))
plot(pca$x, pch=19, col=pamtrue, main="PCA of FQ normalized log-counts", xlab="PC1", ylab="PC2")
```

### Bias of $W\,\alpha_\mu$ estimator

```{r bias_mu}
load("~/git/zinb_analysis/sims/k2_Xintercept_Vintercept_bias.rda")
K <- 1:4

walpha_mu <- plotbias[grepl("walpha_mu", names(plotbias), fixed=TRUE)]
names(walpha_mu) <- sapply(strsplit(names(walpha_mu), ".", fixed=TRUE), function(x) paste(x[1:3], collapse="_"))
boxplot(walpha_mu, outline=FALSE, las=2, main="W * alpha_mu", ylab="Bias")
abline(h=0, col=2)
```

### Bias of $W\,\alpha_\mu$ estimator

```{r bias_mu2}
walpha_mu <- plotbias[grepl("walpha_mu", names(plotbias), fixed=TRUE) &
                      grepl(".X.V.common_disp", names(plotbias), fixed=TRUE)]
names(walpha_mu) <- sapply(strsplit(names(walpha_mu), ".", fixed=TRUE), function(x) paste(x[1], collapse="_"))
boxplot(walpha_mu, outline=TRUE, las=2, main="W * alpha_mu", ylab="Bias")
abline(h=0, col=2)
```

### Bias of $W\,\alpha_\mu$ estimator

```{r bias_mu3}
boxplot(walpha_mu, outline=FALSE, las=2, main="W * alpha_mu", ylab="Bias")
abline(h=0, col=2)
```

### Bias of $W\,\alpha_\pi$ estimator

```{r bias_pi1}
walpha_pi <- plotbias[grepl("walpha_pi", names(plotbias), fixed=TRUE)]
names(walpha_pi) <- sapply(strsplit(names(walpha_pi), ".", fixed=TRUE), function(x) paste(x[1:3], collapse="_"))
boxplot(walpha_pi, outline=FALSE, las=2, main="W * alpha_pi", ylab="Bias")
abline(h=0, col=2)
```

### Bias of $W\,\alpha_\pi$ estimator

```{r bias_pi2}
walpha_pi <- plotbias[grepl("walpha_pi", names(plotbias), fixed=TRUE) &
                        grepl(".X.V.common_disp", names(plotbias), fixed=TRUE)]
names(walpha_pi) <- sapply(strsplit(names(walpha_pi), ".", fixed=TRUE), function(x) paste(x[1], collapse="_"))
boxplot(walpha_pi, outline=TRUE, las=2, main="W * alpha_pi", ylab="Bias")
abline(h=0, col=2)
```

### Bias of $W\,\alpha_\pi$ estimator

```{r bias_pi3}
boxplot(walpha_pi, outline=FALSE, las=2, main="W * alpha_pi", ylab="Bias")
abline(h=0, col=2)
```

### Correlation between true and estimated sample distance

```{r dist}
load("~/git/zinb_analysis/sims/k2_Xintercept_Vintercept_dist.rda")

corr <- sapply(6:10, function(i) rowMeans(sapply(res, function(x) x[[i]])))
colnames(corr) <- c(paste0("zinb k=", 1:4), "FQ PCA (k=2)")
boxplot(corr, main="Correlation between true and estimated sample distances in W", border=c(1, 2, 1, 1), xlab="method", ylab="Correlation", las=2)
```

### Silhouette width of PAM clustering

```{r pam}
sil <- sapply(11:15, function(i) rowMeans(sapply(res, function(x) x[[i]])))
colnames(sil) <- c(paste0("zinb k=", 1:4), "FQ PCA (k=2)")
boxplot(sil, main="Silhouette width of the PAM clustering based on true W", border=c(1, 2, 1, 1),  xlab="method", ylab="Silhouette width", las=2)
```

## Real data (with Fanny)

### Allen dataset

I will mostly use the data from a recent Allen Institute dataset that includes neurons from three cortical layers of the mouse brain.

We know the layer of origin of each cell and we have the clustering results of the original publication.

I will compare the ZINB model to PCA on full-quantile normalized data and to ZIFA (on unnormalized counts).

### Allen data: PCA of unnormalized data

\centering
\includegraphics[width=.8\textwidth]{figures/allen_pca0.png}

### Allen data: PCA of FQ normalized data

\centering
\includegraphics[width=.8\textwidth]{figures/allen_pca1.png}

### Allen data: ZINB on unnormalized data

\centering
\includegraphics[width=.8\textwidth]{figures/allen_zinb1.png}

### Allen data: ZINB parameters interpretation

\centering
\includegraphics[width=.8\textwidth]{figures/allen_zinb2.png}

### Allen data: ZIFA on unnormalized data

\centering
\includegraphics[width=.8\textwidth]{figures/allen_zifa1.png}

### ZIFA vs ZINB

\centering
\includegraphics[width=.8\textwidth]{figures/allen_zifa_zinb.png}

### Adding QC measures as covariates

If the data are really noisy, we can add covariates to account for batch effects and other unwanted variation.

### Cortical data: ZINB, no normalization

\centering
\includegraphics[width=.8\textwidth]{figures/cortical_zinb1.png}

### Cortical data: ZINB, accounting for sample quality

\centering
\includegraphics[width=.8\textwidth]{figures/cortical_zinb2.png}

### Cortical data: ZINB, accounting for sample quality

\centering
\includegraphics[width=.8\textwidth]{figures/cortical_zinb3.png}

### Ongoing and future work

- More simulations: robustness to choice of parameters and to misspecification of the model.
- ZIFA on normalized data: fq messes with zeros...
- Extend the model to differential expression: Wald Test and/or Likelihood Ratio Test.

