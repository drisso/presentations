<!DOCTYPE html>
<html>
  <head>
    <title>Statistical challenges in single-cell RNA-seq</title>
    <meta charset="utf-8">
    <meta name="author" content="Davide Risso" />
    <meta name="date" content="2017-08-24" />
    <link href="libs/remark-css/example.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-theme.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Statistical challenges in single-cell RNA-seq
## Normalization and dimensionality reduction
### Davide Risso
### August 24, 2017

---




# Outline

- Motivation

- Comparing normalization methods in real data 

- Zero-inflation aware dimensionality reduction

---
class: center

## Single-cell signal is intrinsecally low-dimensional

&lt;img src="img/cell_state.png" style="width: 550px;"/&gt;

.right[.small[Wagner, Regev, Yosef (2016)]]

---

# Single-cell signal is noisy

&lt;br&gt;&lt;br&gt;

- High level of technical and biological variance.

- High drop-out rate (zero inflation).

- Amplification bias (high expression outliers).

- Unique Molecular Identifiers (UMIs) help but do not solve all these problems.

&lt;br&gt;&lt;br&gt;&lt;br&gt;

### .center[.red[Problematic with typical workflow: global scaling + log + PCA]]

---
class: inverse, center, middle

# Motivation

## Unwanted variation confounds scRNA-seq data

---
class: center

# Example 1: 10X Genomics PBMC data

&lt;img src="img/10x1.png" style="width: 450px;"/&gt;

.right[.small[https://www.10xgenomics.com/single-cell/]]

---
class: center

# Example 1: 10X Genomics PBMC data

&lt;img src="img/10x2.png" style="width: 550px;"/&gt;

---
class: center

# Example 1: 10X Genomics PBMC data

&lt;img src="img/10x3.png" style="width: 530px;"/&gt;

---
class: center

# Example 1: 10X Genomics PBMC data

&lt;img src="img/10x4.png" style="width: 550px;"/&gt;
&lt;img src="img/10x5.png" style="width: 550px;"/&gt;

---
class: center

# Example 2: 10X Genomics Tcell data

&lt;img src="img/10x6.png" style="width: 550px;"/&gt;

.right[.small[https://support.10xgenomics.com/single-cell-gene-expression/datasets/2.0.1/t_4k]]

---
class: center

# Example 2: 10X Genomics Tcell data

&lt;img src="img/10x_t4k_cor.png" style="width: 800px;"/&gt;

---
class: center

# Example 2: 10X Genomics Tcell data

&lt;img src="img/10x_t4k_cor2.png" style="width: 800px;"/&gt;

---
class: center

# Example 3: SMART-Seq2 Tcell data

&lt;img src="img/smart1.png" style="width: 500px;"/&gt;

.right[.small[Gaublomme et al. (2015)]]

---
class: center

# Example 3: SMART-Seq2 Tcell data

&lt;img src="img/smart2.png" style="width: 550px;"/&gt;

---
class: center

# Example 3: SMART-Seq2 Tcell data

&lt;img src="img/smart3.png" style="width: 550px;"/&gt;

---
class: center

# Example 3: SMART-Seq2 Tcell data

&lt;img src="img/smart4.png" style="width: 550px;"/&gt;

---
class: center

# Example 4: Frozen PBMC (two donors)

.right[.small[Zheng et al. (2017)]]

&lt;img src="img/10x_batch1.png" style="width: 500px;"/&gt;

---
class: center

# Example 4: Frozen PBMC (two donors)

&lt;img src="img/tsne-1.png" style="width: 500px;"/&gt;

---
class: inverse, center, middle

# SCONE

## Comparing normalization methods in real data

### https://bioconductor.org/packages/scone

---
class: center

&lt;img src="img/scone1.jpg" style="width: 800px;"/&gt;

---
class: center

&lt;img src="img/scone_model.png" style="width: 800px;"/&gt;

.right[.small[Risso et al. (2014)]]

---
class: center

&lt;img src="img/scone_model2.png" style="width: 800px;"/&gt;

---
class: center

&lt;img src="img/scone2.jpg" style="width: 800px;"/&gt;

---

# Normalization performance metrics

We have developed eight normalization performance metrics that relate to three aspects of the distribution of gene expression measures:

- Clustering of samples according to factors of wanted and unwanted variation.

    - **BIO_SIL**: group the samples by covariate of interest 
    - **BATCH_SIL**: group the samples by batch
    - **PAM_SIL**: perform unsupervised clustering

and compute average silhouette width.
--

- Association of expression measures with factors of wanted and unwanted variation.

    - **EXP_QC_COR**: correlation with QC measures
    - **EXP_UV_COR**: correlation with negative control genes
    - **EXP_WC_COR**: correlation with positive control genes
    
Maximum squared Spearman correlation.

---

# Normalization performance metrics

- Between-sample distribution of expression measures.

    - **RLE_MED**: Mean squared median RLE
    - **RLE_IQR**: Mean inter-quartile range of RLE
    
Relative Log Expression (RLE) measures are defined as log-ratios of read counts 
to median read counts across samples.

---
class: center

&lt;img src="img/scone3.jpg" style="width: 800px;"/&gt;

---
class: center

# SCONE on 10x PBMC data

&lt;img src="img/scone10x1.png" style="width: 550px;"/&gt;

---
class: center

# SCONE on 10x PBMC data

&lt;img src="img/scone10x2.png" style="width: 550px;"/&gt;

---
class: center

# 10x PBMC data: Raw data

&lt;img src="img/scone10x4.png" style="width: 550px;"/&gt;

---
class: center

# 10x PBMC data: After SCONE normalization

&lt;img src="img/scone10x3.png" style="width: 550px;"/&gt;

---
class: center

# SCONE on SMART-Seq2 Tcell data

&lt;img src="img/scone_smart1.png" style="width: 550px;"/&gt;

---
class: center

# SCONE on SMART-Seq2 Tcell data

&lt;img src="img/scone_smart3.png" style="width: 550px;"/&gt;

---

# Evaluation of SCONE rankings

Independent bulk microarray samples available on GEO on similar Th17 samples.

We computed a set of positive (DE) and negative (non DE) control genes by comparing cytokine activation IL-1beta, IL-6, IL-23 samples to cytokine activation TGF-beta1, IL-6.

--

We then used limma with voom weigths to perform differential expression between the conditions of the Th17 dataset and use the p-values to predict the positive and negative controls from the microarray data.

--

The ranking of normalizations by SCONE was then compared to the ranking by the area under the curve (AUC) of the Receiver Operating Characteristic (ROC) curve.

---
class: center

# Evaluation of SCONE rankings

&lt;img src="img/scone_smart2.png" style="width: 500px;"/&gt;

---
class: inverse, center, middle

# ZINB-WaVE

## Zero-inflation aware dimensionality reduction

### https://bioconductor.org/packages/zinbwave

---
# The ZINB-WaVE model

- General and flexible a zero-inflated negative binomial (ZINB) model.

- Extract low-dimensional signal from the data.

- Accounting for zero inflation (dropouts), over-dispersion, and the count nature of the data.

--

- The model includes a sample-level intercept, which serves as a global-scaling normalization factor.

- Gives the user the ability to include both gene-level and sample-level covariates.

--

- Inclusion of observed and unobserved sample-level covariates enables normalization for complex, non-linear effects (batch effects)

- Gene-level covariates may be used to adjust for sequence composition effects, such as gene length and GC-content effects.

---
# The negative binomial distribution

For any `\(\mu\geq 0\)` and `\(\theta&gt;0\)`, the probability mass function
(PMF) of the negative binomial (NB) distribution is 

$$
f_N(y;\mu,\theta) = \frac{\Gamma(y+\theta)}{\Gamma(y+1)\Gamma(\theta)} \left(\frac{\theta}{\theta+\mu}\right)^{\theta} \left(\frac{\mu}{\mu + \theta}\right)^y, \quad \forall y\in\mathbb{N}.
$$


The mean of the NB distribution is `\(\mu\)` and its variance is:
$$
\sigma^2 = \mu + \frac{\mu^2}{\theta}.
$$
In particular, the NB distribution boils down to a Poisson
distribution when `\(\theta = +\infty\)`.

---

# The zero-inflated negative binomial

&lt;br&gt;&lt;br&gt;

For any `\(\pi\in[0,1]\)`, 
the PMF of the zero-inflated negative binomial (ZINB) distribution is
given by


$$
f(y;\mu,\theta, \pi) = \pi \delta_0(y) + (1 - \pi) f_N(y;\mu,\theta), \quad \forall y\in\mathbb{N},
$$

where `\(\delta_0(\cdot)\)` is the Dirac function. 

&lt;br&gt;&lt;br&gt;

Here, `\(\pi\)` can be interpreted as the probability that a `\(0\)` is observed instead of the actual count, resulting in an inflation of zeros compared to the NB distribution, hence the name ZINB.

---

# The ZINB-WaVE model

Given `\(n\)` samples and `\(J\)` genes, let
`\(Y_{ij}\)` denote the count of gene `\(j\)` (for `\(j=1,\ldots,J\)`) for
sample `\(i\)` (for `\(i=1,\ldots,n\)`). 

--

&lt;img src="img/zinb_schema.jpg" style="width: 800px;"/&gt;

---

# Estimation procedure

To estimate the parameters `\((\alpha, \beta, \gamma, W, \theta)\)`, we follow a penalized maximum likelihood approach, to reduce
overfitting and improve the numerical stability of the optimization
problem in the setting of many parameters.

![](img/likelihood.png)

where `\(\zeta = \log \theta\)` and

![](img/penalty.png)

---

# Estimation procedure

- `\(\beta^0\)` and `\(\gamma^0\)` denote the matrices `\(\beta\)` and
`\(\gamma\)` without the rows corresponding to the intercepts if an
unpenalized intercept is included in the model.

- `\(\|\cdot\|\)` is the Frobenius matrix norm ( `\(\|A\| = \sqrt{\text{tr}(A^*A)}\)`, where `\(A^*\)` denotes the conjugate transpose of `\(A\)`).

- The penalty shrinks the estimated
parameters to `\(0\)`, except for the cell and gene-specific intercepts which are not penalized and the dispersion parameters which are shrunk towards a constant value across genes (cf. edgeR).

- The penalty also ensures that at the optimum `\(W\)` and `\(\alpha^T\)` have orthogonal columns, which is useful for visualization or interpretation of latent factors.

---

# Optimization procedure

The penalized likelihood is not concave, making its maximization computationally challenging. 

&lt;br&gt;&lt;br&gt;

We instead find a local maximum, starting from a smart initialization and iterating a numerical optimization scheme until local convergence.

---

1. Dispersion optimization
     ![](img/dispersion_est.png)

2. Left factor (cell-specific) optimization

     ![](img/left_factors.png)

3. Right factor (sample-specific) optimization

     ![](img/right_factors.png)

4. Orthogonalization

     ![](img/orthogonal.png)

---
class: center

# Simulation results

&lt;img src="img/boxplots.png" style="width: 500px;"/&gt;
&lt;img src="img/legend.png" style="width: 200px;"/&gt;

---
class: center

# Simulation results

&lt;img src="img/sil.png" style="width: 450px;"/&gt;
&lt;img src="img/legend.png" style="width: 200px;"/&gt;

---
class: center

# Real data

&lt;img src="img/realdata.jpg" style="width: 600px;"/&gt;

.right[.small[a. Tasic et al. (2016); b. Zeisel et al. (2015); c. Patel et al. (2014); d. Kolodziejczyk et al. (2015)]]
---
class: center

# 10X Genomics Tcell data: PCA

&lt;img src="img/10x_t4k_cor2.png" style="width: 800px;"/&gt;

---
class: center

# 10X Genomics Tcell data: ZINB-WaVE

&lt;img src="img/10x_t4k_cor3.png" style="width: 800px;"/&gt;

---
class: center

# ZINB-WaVE: pct_ribo as covariate

&lt;img src="img/10x_t4k_cor4.png" style="width: 800px;"/&gt;

---
class: center

# 10X Genomics 4k PBMC data: PCA

&lt;img src="img/pbmc1.png" style="width: 500px;"/&gt;

---
class: center

# 10X Genomics 4k PBMC data: ZINB-WaVE

&lt;img src="img/pbmc2.png" style="width: 500px;"/&gt;

---
class: center

# 10X Genomics 4k PBMC data: ZINB-WaVE

&lt;img src="img/pbmc3.png" style="width: 500px;"/&gt;

---
class: center

# 10X Genomics 4k PBMC data: ZINB-WaVE

&lt;img src="img/pbmc4.png" style="width: 500px;"/&gt;

---
class: center

# 10X Genomics Frozen PBMC data

&lt;img src="img/tsne-1.png" style="width: 550px;"/&gt;

---
class: center

# 10X Genomics Frozen PBMC data

&lt;img src="img/tsne_zinb-1.png" style="width: 550px;"/&gt;

---
class: center

# 10X Genomics Frozen PBMC data

&lt;img src="img/tsne_zinb-2.png" style="width: 550px;"/&gt;

---

# Summing up

- SCONE is a general framework to test and compare normalization methods on real data.

- No single normalization method is universally optimal across platforms and protocols

- SCONE can be used to either directly normalize the data or to choose the appropriate covariates to include in a model-based approach (ZINB-WaVE, MAST, ...)

--

- ZINB-WaVE is a general and flexible method for low-dimensional signal extraction from noisy, zero-inflated count data.

- It works well for projecting single-cell data in two dimensions as a dimensionality reduction step prior to clustering or pseudotime inference.

---
.pull-right[
# Acknowledgements
]

### SCONE

Michael B. Cole, Allon Wagner, Nir Yosef

Elizabeth Purdom, Sandrine Dudoit


.pull-right[
### ZINB-WaVE

Fanny Perradeau, Sandrine Dudoit

Svetlana Gribkova

Jean-Philippe Vert

]


---
class: center, middle

# Thank you!

.pull-left[
`scone` package: 

[bioconductor.org/packages/scone](https://bioconductor.org/packages/scone)

&lt;/br&gt;

&lt;/br&gt;

&lt;/br&gt;

.left[
Contacts:

Github: [@drisso](https://github.com/drisso)

Twitter: [@drisso1893](https://twitter.com)

Email: [`dar2062@med.cornell.edu`](mailto:dar2062@med.cornell.edu)
]
]

.pull-right[
ZINB-Wave preprint: 

[doi.org/10.1101/125112](https://doi.org/10.1101/125112)

`zinbwave` package: 

[bioconductor.org/packages/zinbwave](https://bioconductor.org/packages/zinbwave)

&lt;img src="img/zinbwave.png" style="width: 250px;"/&gt;

]
    </textarea>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {window.dispatchEvent(new Event('resize'));});</script>

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  tex2jax: {
    skipTags: ['script', 'noscript', 'style', 'textarea', 'pre']
  }
});
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://cdn.bootcss.com/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
